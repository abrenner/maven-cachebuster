# Maven Cache Buster

A maven plugin to generate cache busting files

# Compile and Installation

Download the source code and run the standard maven workflow.

    $ mvn clean install

# Usage

Add the following to your project pom.xml file within the build
section.

```java
<plugin>
    <groupId>io.aeb.oss.mvn.cachebuster</groupId>
    <artifactId>maven-cachebuster</artifactId>
    <version>1.0.0</version>         
    <configuration>
      <sourceDirectory>${basedir}/css/</sourceDirectory>
      <outputDirectory>${basedir}/cache/css/</outputDirectory>
      <mappingFile>${basedir}/cache/mapping.json</mappingFile>
    </configuration>
    <executions>
        <execution>
            <id>create-cache-busting-files</id>
            <phase>process-resources</phase>
            <goals>
              <goal>CacheBuster</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

During the process-resources phases this plugin will copy all files
(css, javascript, images, etc.) from the sourceDirectory configuration
to the outputDirectory configuration and append a SHA-1 hash with the
first seven characters (digest) to each file.

A json mapping file will be produced in the mappingFile configuration
which will map the non-cache busted file to the cache busted file. This
file may be used within your project to map the two entries.
