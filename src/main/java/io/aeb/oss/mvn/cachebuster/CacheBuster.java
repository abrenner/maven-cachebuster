package io.aeb.oss.mvn.cachebuster;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FilenameUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo (name = "CacheBuster", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class CacheBuster extends AbstractMojo
{

    @Parameter (defaultValue = "${basedir}/src/main/resources/", required = true)
    private File sourceDirectory;

    @Parameter (defaultValue = "${project.build.directory}/cache", required = true)
    private File outputDirectory;

    @Parameter (defaultValue = "${project.build.directory}/cache/mapping.json", required = true)
    private File mappingFile;

    private Map<String, String> map = new HashMap<String, String>();

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        if (!isValidDirectory(sourceDirectory)) {
            getLog().error("Directory: "+sourceDirectory+" does not exists");
            return;
        }

        if(!createDirectory(outputDirectory)) {
            getLog().error("Unable to create directory: " + outputDirectory);
        } else {
            getLog().info("Created directory: " + outputDirectory);
        }

        getLog().info("Directory: "+sourceDirectory+" exists");
        MessageDigest md = generateMessageDigest("SHA-1");

        for (File file : sourceDirectory.listFiles()) {
            String checksum = generateChecksum(md, new File(sourceDirectory+File.separator+file.getName()));
            String newName = FilenameUtils.removeExtension(file.getName()) + "-" + checksum + "." + FilenameUtils.getExtension(file.getName());
            copyFile(file, new File(outputDirectory.getAbsolutePath()+File.separator+newName));
            map.put(file.getName(), newName);
        }

        generateMapFile();
    }

    private boolean isValidDirectory(File file)
    {
        if (file.exists() && file.isDirectory())
            return true;
        return false;
    }

    private boolean createDirectory(File file)
    {
        if(isValidDirectory(file))
            return true;
        return file.mkdirs();
    }

    private void copyFile(File src, File dst) throws MojoFailureException
    {
        try {
            getLog().info("Cache busting: " + src + " to: "+ dst);
            Files.copy(src.toPath(), dst.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new MojoFailureException("IO error");
        }
    }

    private void generateMapFile() throws MojoFailureException
    {
        try {
            String output = "{";
            boolean isFirst = true;
            for(String key : map.keySet()) {
                if(!isFirst) output += ",";
                isFirst = false;
                output += "\"" + key + "\":\""+map.get(key)+"\"";
            }
            output += "}";
            BufferedWriter writer = new BufferedWriter(new FileWriter(mappingFile, false));
            writer.append(output);
            writer.close();
        } catch (IOException e) {
            throw new MojoFailureException("Could not write to mapping file");
        }
    }

    private MessageDigest generateMessageDigest(String alg) throws MojoFailureException {
        try {
            return MessageDigest.getInstance(alg);
        } catch (NoSuchAlgorithmException e) {
            throw new MojoFailureException("Could not load algorthim "+alg+" on system");
        }
    }

    private String generateChecksum(MessageDigest md, File file) throws MojoFailureException {
        try {
            md.update(Files.readAllBytes(Paths.get(file.toURI())));
            return DatatypeConverter.printHexBinary(md.digest()).toLowerCase().substring(0, 7);
        } catch (IOException e) {
            throw new MojoFailureException("Could not load file: "+file);
        }
    }
}